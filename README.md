# dependency-checker-core

Package for core dependency checker logic used by higher level abstractions.

Given a set of data, meta data and dependencies, this package outputs the effects of applying the dependencies on the data.

## Example

```js
const data = {
  A: {
    firstName: 'Benjamin',
    lastName: 'Test',
    age: 120,
  },
  B: {
    isCurious: true,
  },
};

const metaData = {
  A: {},
  B: {
    shouldApplyEffects: true,
  },
};

const dependencies = {
  A: [
    {
      key: 'B',
      type: 'left out for brevity',
      // data and metaData for "B"
      cond: (data, metaData) => data.isCurious && metaData.shouldApplyEffects,
      effects: {
        lastName: 'Button',
      },
    },
    {
      key: 'A',
      type: 'left out for brevity',
      // data and metaData for "A"
      cond: (data, metaData) => data.lastName === 'Button',
      effects: {
        age: 20,
      },
    },
  ],
};
```

The output of applying the above dependencies to the above data and meta data is the following:

```js
const updatedData = {
  A: {
    firstName: 'Benjamin',
    lastName: 'Button',
    age: 20,
  },
  B: {
    isCurious: true,
  },
};
```

## Dependencies

### Dependency Properties

Dependencies have the following properties:

```js
const dependencies = {
  // A list of dependencies for the item "A"
  A: [
    {
      // the key that a given item is dependent on (i.e. "A" is dependent on "B")
      key: 'B',
      // A value used for TypeScript support that gives completion for the `data` and `metaData` below
      type: 'some-type',
      // A condition that gets the data and metaData for "B"
      // If the condition evaluates to true then the effects
      // are applied, otherwise the effects are ignored
      cond: (data, metaData) => true,
      // Properties which overwrite the data for "A"
      // effects stack, so a dependency lower in the list
      // with the same effect property will take precedence
      effects: {},
    },
  ],
};
```

### Dependency Stacking

Since the dependencies are an array, you may be wondering what happens when two dependencies that update the same data property exist. In this case, the order of the dependencies comes into play with dependencies coming later in the list taking priority.

### Dependency Chaining

Complex dependency chains can be made between n-number of items. For example:

```js
const dependencies = {
  B: [
    {
      key: 'A',
      type: 'some-type',
      cond: () => true,
      effects: {},
    },
  ],
  C: [
    {
      key: 'B',
      type: 'some-type',
      cond: () => true,
      effects: {},
    },
  ],
};
```

In this scenario, item "B" is dependent on item "A" and item "C" is dependent on item "B". If the item "B" dependency on item "A" evaluates to true, a recalculation of item "C" will be triggered since it's dependent on item "B".

### Self Dependency

Items can be dependent on themselves which can be useful for various things such as error handling. For example:

```js
const dependencies = {
  A: [
    {
      key: 'A',
      type: 'some-type',
      cond: (data) => data.value === "I'm invalid",
      effects: {
        error: true,
      },
    },
  ],
};
```

## Limitations

As of today, the dependency checker does not support circular dependencies, so "A" can't be dependent on "B" and "B" can't be dependent on "A" at the same time.
