import { createFileConfig, createExternalDeps } from '@archer-oss/dev-scripts/src/rollup.config';
export default createFileConfig({ input: 'src/dependency-checker.ts', external: createExternalDeps({ '@babel/runtime': 'version' }) });
