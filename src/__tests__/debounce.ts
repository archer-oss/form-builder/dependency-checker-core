import { debounce } from '../utils';

it.each`
  wait         | numTimesFnCalled | numTimesSetTimeoutCalled | numTimesClearIntervalCalled
  ${100}       | ${1}             | ${100}                   | ${100}
  ${500}       | ${1}             | ${100}                   | ${100}
  ${0}         | ${100}           | ${0}                     | ${0}
  ${undefined} | ${100}           | ${0}                     | ${0}
`(
  'should handle the debounce util correctly when the wait is $wait',
  ({
    wait,
    numTimesFnCalled,
    numTimesSetTimeoutCalled,
    numTimesClearIntervalCalled,
  }: {
    wait: number;
    numTimesFnCalled: number;
    numTimesSetTimeoutCalled: number;
    numTimesClearIntervalCalled: number;
  }) => {
    jest.useFakeTimers();
    const normalFn = jest.fn();
    const debouncedFn = debounce(normalFn, { wait });

    const setTimeoutSpy = jest.spyOn(global, 'setTimeout');
    const clearIntervalSpy = jest.spyOn(global, 'clearInterval');

    for (let i = 0; i < 100; i++) {
      debouncedFn(i);
    }

    jest.advanceTimersToNextTimer();

    expect(normalFn).toHaveBeenCalledTimes(numTimesFnCalled);
    expect(normalFn).toHaveBeenLastCalledWith(99);
    expect(setTimeoutSpy).toHaveBeenCalledTimes(numTimesSetTimeoutCalled);
    expect(clearIntervalSpy).toHaveBeenCalledTimes(numTimesClearIntervalCalled);
    jest.useRealTimers();
  },
);

it('should allow an optional argsModifier to control the args which are passed to the underlying function', () => {
  jest.useFakeTimers();
  const normalFn = jest.fn();

  // The arguments passed to the final
  // function call should be unchanged
  const debouncedFn = debounce(normalFn, { wait: 100 });

  debouncedFn(1, 2);
  debouncedFn(3, 4);

  jest.advanceTimersToNextTimer();

  expect(normalFn).toHaveBeenCalledTimes(1);
  expect(normalFn).toHaveBeenCalledWith(3, 4);

  // The arguments passed to the final function
  // call should be changed by the modifier
  normalFn.mockClear();
  const debouncedFnWithArgsModifier = debounce(normalFn, {
    wait: 100,
    // Combines a and b from each invocation
    argsModifier: (argsArr: [number, number][]) => {
      return argsArr.reduce(([combinedA, combinedB], [a, b]) => [combinedA + a, combinedB + b]);
    },
  });

  debouncedFnWithArgsModifier(1, 2);
  debouncedFnWithArgsModifier(3, 4);
  debouncedFnWithArgsModifier(5, 6);

  jest.advanceTimersToNextTimer();

  expect(normalFn).toHaveBeenCalledTimes(1);
  expect(normalFn).toHaveBeenCalledWith(9, 12);
  jest.useRealTimers();
});
