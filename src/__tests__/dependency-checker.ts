import { createDependencyCheckerHandler, CreateDependenciesTypes } from '../dependency-checker';

describe('Dependency Checker', () => {
  const dependencyData = createDependencyData();

  const data: Record<string, DependencyConfig[keyof DependencyConfig]['data']> = {};
  const dependencies: Record<string, DependencyConfig[keyof DependencyConfig]['dependencies']> = {};
  Object.keys(dependencyData).forEach((key) => {
    data[key] = dependencyData[key].data;
    dependencies[key] = dependencyData[key].dependencies;
  });

  // Dependency Chain: key-one --(depends on)--> key-one (self-dependency)
  it('returns the expected effects for key-one when key-one changes', async () => {
    const dependencyChecker = createAsyncDependencyChecker();
    const results = await dependencyChecker(dependencies, ['key-one'], { ...data, 'key-one': { isValid: false } }, {}, {});
    expect(results.pendingEffects).toEqual({ 'key-one': { name: 'key one effect applied' } });
  });

  // Dependency Chain: key-one --(depends on)--> key-two
  it('returns the expected effects for key-one when key-two changes', async () => {
    const dependencyChecker = createAsyncDependencyChecker();
    const results = await dependencyChecker(dependencies, ['key-two'], { ...data, 'key-two': { age: 21 } }, {}, {});
    expect(results.pendingEffects).toEqual({ 'key-one': { name: 'key two effect applied' } });
  });

  // Dependency Chain: key-one --(depends on)--> key-three (effect one)
  it('returns the expected effects for key-one when key-three changes (effect one)', async () => {
    const dependencyChecker = createAsyncDependencyChecker();
    const results = await dependencyChecker(dependencies, ['key-three'], { ...data, 'key-three': { person: { age: 30 } } }, {}, {});
    expect(results.pendingEffects).toEqual({ 'key-one': { name: 'key three effect one applied' } });
  });

  // Dependency Chain: key-one --(depends on)--> key-three (effect two overwrites effect one)
  it('returns the expected effects for key-one when key-three changes (effect two)', async () => {
    const dependencyChecker = createAsyncDependencyChecker();
    const results = await dependencyChecker(
      dependencies,
      ['key-three'],
      { ...data, 'key-three': { person: { age: 30, name: 'key one effect' } } },
      {},
      {},
    );
    expect(results.pendingEffects).toEqual({ 'key-one': { name: 'key three effect two applied' } });
  });

  // Dependency Chain: key-one --(depends on)--> key-two --(depends on)--> key-three
  it('returns the expected effects for key-one and key-two when key-three changes', async () => {
    const dependencyChecker = createAsyncDependencyChecker();
    const results = await dependencyChecker(dependencies, ['key-three'], { ...data, 'key-three': { person: { age: 20 } } }, {}, {});
    expect(results.pendingEffects).toEqual({ 'key-one': { name: 'key two effect applied' }, 'key-two': { age: 21 } });
  });

  it('marks keys as having stale effects if no dependency conditions evaluate to true', async () => {
    const dependencyChecker = createAsyncDependencyChecker();
    const results = await dependencyChecker(
      dependencies,
      ['key-three'],
      { ...data, 'key-one': { isValid: true } },
      {},
      { 'key-one': { name: 'key three effect applied' }, 'key-two': { age: 21 } },
    );
    expect(results.staleDependentKeys).toEqual(expect.arrayContaining(['key-one', 'key-two']));
  });

  // Dependency Chain:
  //    - key-one --(depends on)--> key-one (self-dependency)
  //    - key-four --(depends on)--> key-four (self-dependency)
  it('returns the expected effects for key-one and key-four when both change with a debounced dependency checker', async () => {
    const dependencyChecker = createAsyncDependencyChecker(100);

    // This first call will be debounced
    dependencyChecker(dependencies, ['key-four'], { ...data, 'key-one': { isValid: false }, 'key-four': { isValid: false } }, {}, {});
    const results = await dependencyChecker(
      dependencies,
      ['key-one'],
      { ...data, 'key-one': { isValid: false }, 'key-four': { isValid: false } },
      {},
      {},
    );
    expect(results.pendingEffects).toEqual({ 'key-one': { name: 'key one effect applied' }, 'key-four': { name: 'key four self dependency' } });
  });
});

/**
 * Utilities
 */
type BaseDependencyConfig = {
  ['type-one']: { type: 'type-one'; data: { name?: string; isValid?: boolean }; metaData: Record<string, any> };
  ['type-two']: { type: 'type-two'; data: { age?: number; isValid?: boolean }; metaData: Record<string, any> };
  ['type-three']: {
    type: 'type-three';
    data: { person?: { name?: string; age?: number; isValid?: boolean } };
    metaData: Record<string, any>;
  };
  ['type-four']: { type: 'type-four'; data: { name?: string; isValid?: boolean }; metaData: Record<string, any> };
};

type Dependencies = CreateDependenciesTypes<BaseDependencyConfig>;

type DependencyConfig = {
  [key in keyof BaseDependencyConfig]: BaseDependencyConfig[key] & { key: string; dependencies: Dependencies };
};

function createAsyncDependencyChecker(dependencyCheckerRate = 0) {
  const dependencyChecker = createDependencyCheckerHandler(dependencyCheckerRate);

  return (
    dependencies: Record<string, DependencyConfig[keyof DependencyConfig]['dependencies']>,
    modifiedKeys: string[],
    currentData: Record<string, DependencyConfig[keyof DependencyConfig]['data']>,
    currentMetaData: Record<string, Record<string, any>>,
    currentEffects: Record<string, Record<string, any>>,
  ) => {
    return new Promise<any>((resolve) => {
      dependencyChecker.handler({
        dependencies,
        modifiedKeys,
        currentData,
        currentMetaData,
        currentEffects,
        onDependencyCheckerComplete: (pendingEffects, staleDependentKeys) => resolve({ pendingEffects, staleDependentKeys }),
      });
    });
  };
}

function createDependencyData() {
  const dependencyData: Record<string, DependencyConfig[keyof DependencyConfig]> = {
    'key-one': {
      key: 'key-one',
      type: 'type-one',
      data: {},
      metaData: {},
      dependencies: [
        {
          key: 'key-one',
          type: 'type-one',
          cond: (data) => !data.isValid,
          effects: { name: 'key one effect applied' },
        },
        {
          key: 'key-two',
          type: 'type-two',
          cond: (data) => (data.age || 0) === 21,
          effects: { name: 'key two effect applied' },
        },
        {
          key: 'key-three',
          type: 'type-three',
          cond: (data) => (data.person?.age || 0) === 30,
          effects: { name: 'key three effect one applied' },
        },
        {
          key: 'key-three',
          type: 'type-three',
          cond: (data) => (data.person?.name || '') === 'key one effect',
          effects: () => ({ name: 'key three effect two applied' }),
        },
      ],
    },
    'key-two': {
      key: 'key-two',
      type: 'type-two',
      data: {},
      metaData: {},
      dependencies: [{ key: 'key-three', type: 'type-three', cond: (data) => (data.person?.age || 0) === 20, effects: { age: 21 } }],
    },
    'key-three': {
      key: 'key-three',
      type: 'type-three',
      data: {},
      metaData: {},
      dependencies: [],
    },
    'key-four': {
      key: 'key-four',
      type: 'type-four',
      data: {},
      metaData: {},
      dependencies: [
        {
          key: 'key-four',
          type: 'type-four',
          cond: (data) => !data.isValid,
          effects: { name: 'key four self dependency' },
        },
      ],
    },
  };

  return dependencyData;
}
