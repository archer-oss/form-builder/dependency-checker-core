import { uniq } from '../utils';

it.each`
  initialArray                               | filteredArray
  ${[1, 2, 3, 4, 3, 7, 8, 7, 3]}             | ${[1, 2, 3, 4, 7, 8]}
  ${['a', 9, true, 88, 'b', 'b', 'b', 88]}   | ${['a', 9, true, 88, 'b']}
  ${[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]} | ${[1]}
`(
  'should handle the uniq util correctly for the array $initialArray',
  ({ initialArray, filteredArray }: { filteredArray: any[]; initialArray: any[] }) => {
    const uniqArray = uniq(initialArray);
    expect(uniqArray).toHaveLength(filteredArray.length);
    expect(uniqArray).toEqual(filteredArray);
  },
);
