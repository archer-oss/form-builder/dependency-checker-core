/**
 * Wraps a function and returns a debounced version and keeps
 * track of all the arguments for each function invocation.
 */
type DebounceOptions<Args extends any[]> = { wait?: number; argsModifier?: (args: Args[]) => Args };
export function debounce<Args extends any[]>(fn: (...args: Args) => void, { wait, argsModifier }: DebounceOptions<Args>) {
  const skipDebounce = wait === undefined || wait === 0;
  if (skipDebounce) return fn;

  const argsArr: Args[] = [];
  let timer: ReturnType<typeof setTimeout>;

  return (...args: Args) => {
    // Keeps track of each invocation's set of args
    argsArr.push(args);
    clearInterval(timer);

    timer = setTimeout(() => {
      // Modify the array of args passed to the function
      // based on each of the function invocations.
      const fnArgs = argsModifier?.(argsArr) ?? args;
      fn(...fnArgs);
    }, wait);
  };
}

/**
 * Recursively merges objects together
 */
function isBasic(value: any) {
  const type = typeof value;
  return (
    value === null ||
    type === 'bigint' ||
    type === 'boolean' ||
    type === 'function' ||
    type === 'number' ||
    type === 'string' ||
    type === 'symbol' ||
    type === 'undefined'
  );
}

const isArray = (value: any): value is any[] => Array.isArray(value);

const isObject = (value: any): value is Record<string, any> => value !== null && !isArray(value) && typeof value === 'object';

function deepClone(value: any): any {
  if (isBasic(value)) return value;
  if (isArray(value)) return value.map(deepClone);
  if (isObject(value)) return Object.entries(value).reduce((clonedObj, [key, value]) => ({ ...clonedObj, [key]: deepClone(value) }), {});
}

function baseMerge(destination: any, sourceKey: string | number, sourceValue: any) {
  // Don't overwrite the destination if the source is undefined
  if (sourceValue === undefined) return;

  const destinationValue = destination[sourceKey];

  if (
    isBasic(destinationValue) ||
    isBasic(sourceValue) ||
    // Overwrite the value if the type between the destination and source doesn't match
    (isArray(destinationValue) && isObject(sourceValue)) ||
    (isObject(destinationValue) && isArray(sourceValue))
  ) {
    destination[sourceKey] = deepClone(sourceValue);
  } else if (isArray(destinationValue) && isArray(sourceValue)) {
    sourceValue.forEach((item, index) => baseMerge(destinationValue, index, item));
  } else if (isObject(destinationValue) && isObject(sourceValue)) {
    Object.entries(sourceValue).forEach(([key, value]) => baseMerge(destinationValue, key, value));
  }
}

export function merge(destination: Record<string, any>, ...sources: Record<string, any>[]) {
  sources.forEach((source) => Object.entries(source).forEach(([key, value]) => baseMerge(destination, key, value)));
  return destination;
}

/**
 * Takes an array and returns a new array with any duplicate elements removed
 */
export function uniq(arr: any[]) {
  return [...new Set(arr)];
}
