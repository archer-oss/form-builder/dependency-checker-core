type RecursivePartial<T> = {
  [P in keyof T]?: T[P] extends (infer U)[] ? RecursivePartial<U>[] : T[P] extends object ? RecursivePartial<T[P]> : T[P];
};

type BaseDependencyTypes = {
  [Type: string]: {
    type: typeof Type;
    data: BaseData;
    metaData: BaseMetaData;
  };
};

export type CreateDependenciesTypes<DependencyTypes extends BaseDependencyTypes> = {
  [DependentType in keyof DependencyTypes]: {
    key: string;
    type: DependencyTypes[DependentType]['type'];
    cond: (data: Partial<DependencyTypes[DependentType]['data']>, metaData: Partial<DependencyTypes[DependentType]['metaData']>) => boolean;
    effects:
      | RecursivePartial<BaseData>
      | ((
          data: Partial<DependencyTypes[DependentType]['data']>,
          metaData: Partial<DependencyTypes[DependentType]['metaData']>,
        ) => RecursivePartial<BaseData>);
  };
}[keyof DependencyTypes][];

export type BaseData = Record<string, any>;
export type BaseMetaData = Record<string, any>;
export type BaseEffects = Record<string, any>;
export type BaseDependencies = CreateDependenciesTypes<BaseDependencyTypes>;

export type DataCollection = Record<string, BaseData>;
export type MetaDataCollection = Record<string, BaseMetaData>;
export type EffectsCollection = Record<string, BaseEffects>;
export type DependenciesCollection = Record<string, BaseDependencies>;

export type DependencyCheckerHandlerArgs = {
  dependencies: DependenciesCollection;
  modifiedKeys: string[];
  currentData: DataCollection;
  currentMetaData: MetaDataCollection;
  currentEffects: EffectsCollection;
  onDependencyCheckerComplete: (pendingEffects: EffectsCollection, staleDependentKeys: string[]) => void;
};
