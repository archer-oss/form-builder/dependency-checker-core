import { debounce, merge, uniq } from './utils';
import type {
  DataCollection,
  MetaDataCollection,
  EffectsCollection,
  DependenciesCollection,
  DependencyCheckerHandlerArgs,
} from './dependency-checker.type';

/**
 * Converts all the dependencies into a format the dependency checker can more easily consume
 * Example:
 *  Before:
 *    {
 *      'Key-A': { dependencies: [{ key: 'Key-B' }, { key: 'Key-C' }] },
 *      'Key-B': { dependencies: [{ key: 'Key-C' }] },
 *      'Key-C': { dependencies: [] },
 *    }
 *  After:
 *    {
 *      'Key-B': ['Key-A'],
 *      'Key-C': ['Key-A', 'Key-B']
 *    }
 * Anytime 'Key-C' changes, we know 'Key-A' and 'Key-B' need to recalculate
 * @param dependentConfigs A hash of all the dependencies for every key
 */
function normalizeDependencyKeys(dependentConfigs: DependenciesCollection) {
  const normalizedDependencyKeys: Record<string, string[]> = {};
  const dependentConfigKeys = Object.keys(dependentConfigs);
  for (const dependentKey of dependentConfigKeys) {
    const dependencies = dependentConfigs[dependentKey] || [];
    for (const dependency of dependencies) {
      const { key } = dependency;
      if (!normalizedDependencyKeys[key]) {
        normalizedDependencyKeys[key] = [dependentKey];
      } else if (!normalizedDependencyKeys[key].includes(dependentKey)) {
        normalizedDependencyKeys[key].push(dependentKey);
      }
    }
  }
  return normalizedDependencyKeys;
}

/**
 * Given a modified key, this function will recalculate any other key that is dependent on that modified key. It returns any pending effects which
 * need to be committed, a list of dependent keys with stale effects which should be flushed and a list of the next keys which need to be recalculated.
 * @param dependencies A hash of all the dependencies for every key
 * @param normalizedDependencyKeys A normalized hash of dependency information created using the normalizeDependencyKeys function
 * @param modifiedKey The key that triggered the recalculation of dependencies
 * @param pendingEffects All the effects that have been calculated in the current recalculation cycle that have yet to be commited
 * @param staleDependentKeys All the keys that are marked as stale (i.e their effects should be ignored in any calculations)
 * @param currentData A hash of all the current data for every key
 * @param currentMetaData A hash of all the current meta data for every key
 * @param currentEffects A hash of all the existing effects for every key
 */
function calculateDependencies(
  dependencies: DependenciesCollection,
  normalizedDependencyKeys: ReturnType<typeof normalizeDependencyKeys>,
  modifiedKey: string,
  pendingEffects: EffectsCollection,
  staleDependentKeys: string[],
  currentData: DataCollection,
  currentMetaData: MetaDataCollection,
  currentEffects: EffectsCollection,
) {
  // Get all the keys that are dependent on the modified key
  const dependentKeys = normalizedDependencyKeys[modifiedKey] || [];

  // Create a copy of the pending effects to avoid mutating the original object
  // An effect is considered pending if it was created in the same recalculation cycle
  const modifiedPendingEffects = { ...pendingEffects };

  // Keeps track of all the dependent keys that updated to schedule a recalculation of those keys' own dependent keys
  // Recalculations scheduled in this manner are considered to be part of the same recalculation cycle
  const nextKeysToCalculate: string[] = [];

  // Create a copy of the stale dependent keys to avoid mutating the original array
  // A key is considered stale (i.e. existing effects should be flushed) if all dependency conditions evalute to false
  let modifiedStaleDependentKeys = [...staleDependentKeys];

  // Recalculate the effects for each key that is dependent on the modified key
  for (const dependentKey of dependentKeys) {
    // Delete any pending effects for the dependent key to flush stale data before starting the recalculation
    // This is important for any key that depends on itself to ensure the recalculation starts from a clean slate
    delete modifiedPendingEffects[dependentKey];

    // Mark the dependent key as stale if there are any existing effects since we're about to recalculate the effects
    // If any of the depenent key's own dependency conditions evaluate to true, the key will no longer be considered stale
    if (currentEffects[dependentKey]) {
      modifiedStaleDependentKeys.push(dependentKey);
    }

    // Evaluate each dependency's condition for a specific dependentKey
    const dependentKeyDependencies = dependencies[dependentKey] || [];
    for (const dependency of dependentKeyDependencies) {
      // Effects are merged with data before performing any calculations to ensure an accurate result is returned
      // Pending effects are given priority over any current effects
      // The current effects are ignored if they are scheduled to be removed (i.e. the dependent key is stale)
      let effectsToMerge = modifiedPendingEffects[dependency.key];
      const areCurrentDependencyEffectsStale = modifiedStaleDependentKeys.includes(dependency.key);
      if (!effectsToMerge && !areCurrentDependencyEffectsStale) {
        effectsToMerge = currentEffects[dependency.key];
      }

      // Merge the effects with the data for a specific key before the condition is checked
      const { data = {} } = merge({}, { data: currentData[dependency.key] }, { data: effectsToMerge });

      // Merge and store the dependency effects if the dependency condition evaluates to true
      if (dependency.cond(data, currentMetaData[dependency.key] || {})) {
        merge(modifiedPendingEffects, {
          [dependentKey]:
            typeof dependency.effects === 'function' ? dependency.effects(data, currentMetaData[dependency.key] || {}) : dependency.effects,
        });

        // Mark the dependentKey as modified to schedule the recalculation of this key's own dependent keys (i.e. recalculate the entire dependency chain)
        // Don't recalculate the key if the dependentKey and modifiedKey are the same (i.e. self-dependency)
        if (dependentKey !== modifiedKey) {
          nextKeysToCalculate.push(dependentKey);
        }

        // The dependentKey is no longer considered stale because we need to apply the effects related to the condition which just evaluated to true
        modifiedStaleDependentKeys = modifiedStaleDependentKeys.filter((key) => key !== dependentKey);
      }
    }

    // If the dependentKey is marked as stale, schedule the recalculation of this key's own dependent keys (i.e. recalculate the entire dependency chain)
    // Don't recalculate the key if the dependentKey and modifiedKey are the same (i.e. self-dependency)
    if (modifiedStaleDependentKeys.includes(dependentKey) && dependentKey !== modifiedKey) {
      nextKeysToCalculate.push(dependentKey);
    }
  }

  return {
    modifiedPendingEffects,
    modifiedStaleDependentKeys,
    nextKeysToCalculate,
  };
}

/**
 * A function that takes a hash of dependency information, a list of keys that need to be calculated, all the current information about those keys and a
 * callback function that is invoked when the calculation is completed. The callback takes the pending effects and a list of stale keys as its arguments.
 * @param dependencies A hash of all the dependencies for every key
 * @param modifiedKeys A list of keys whose dependency chains need to be recalculated
 * @param currentData A hash of all the current data for every key
 * @param currentMetaData A hash of all the current meta data for every key
 * @param currentEffects A hash of all the existing effects for every key
 */
export function dependencyChecker(
  dependencies: DependenciesCollection,
  modifiedKeys: string[],
  currentData: DataCollection,
  currentMetaData: MetaDataCollection,
  currentEffects: EffectsCollection,
) {
  const normalizedDependencyKeys = normalizeDependencyKeys(dependencies);
  let pendingEffects: EffectsCollection = {};
  let staleDependentKeys: string[] = [];
  let keysToCalculate = [...modifiedKeys];

  while (keysToCalculate.length > 0) {
    let nextModifiedKeys: string[] = [];

    // eslint-disable-next-line no-loop-func
    keysToCalculate.forEach((modifiedKey) => {
      const { modifiedPendingEffects, modifiedStaleDependentKeys, nextKeysToCalculate } = calculateDependencies(
        dependencies,
        normalizedDependencyKeys,
        modifiedKey,
        pendingEffects,
        staleDependentKeys,
        currentData,
        currentMetaData,
        currentEffects,
      );

      // Update the pendingEffects, staleDependentKeys and nextModifiedKeys after each key is calculated
      pendingEffects = modifiedPendingEffects;
      staleDependentKeys = uniq(modifiedStaleDependentKeys);
      // Remove any duplicates if a key was scheduled to recalculate multiple times
      // e.g. A single key is dependent on two other keys that both recalculated
      nextModifiedKeys = uniq([...nextModifiedKeys, ...nextKeysToCalculate]);
    });
    keysToCalculate = nextModifiedKeys;
  }

  return { pendingEffects, staleDependentKeys };
}

// If a dependencyCheckerRate is not provided then the check is synchronous
export function createDependencyCheckerHandler(dependencyCheckerRate?: number) {
  let hasCanceled = false;

  function handler({
    dependencies,
    modifiedKeys,
    currentData,
    currentMetaData,
    currentEffects,
    onDependencyCheckerComplete,
  }: DependencyCheckerHandlerArgs) {
    const { pendingEffects, staleDependentKeys } = dependencyChecker(dependencies, modifiedKeys, currentData, currentMetaData, currentEffects);
    if (!hasCanceled) {
      onDependencyCheckerComplete(pendingEffects, staleDependentKeys);
    }
  }

  return {
    handler: debounce(handler, {
      wait: dependencyCheckerRate,
      argsModifier: (args) => {
        return args.reduce(([fullHandlerArgs], [nextHandlerArgs]) => [
          {
            ...nextHandlerArgs,
            // Aggregate the modified keys to allow for debounced calls to be calculated accurately
            modifiedKeys: uniq([...fullHandlerArgs.modifiedKeys, ...nextHandlerArgs.modifiedKeys]),
          },
        ]);
      },
    }),
    cancel: () => {
      hasCanceled = true;
    },
  };
}

/* Re-export utils */
export { debounce, merge, uniq };

/* Re-export types */
export type {
  BaseData,
  BaseMetaData,
  BaseEffects,
  BaseDependencies,
  DataCollection,
  MetaDataCollection,
  EffectsCollection,
  DependenciesCollection,
  CreateDependenciesTypes,
  DependencyCheckerHandlerArgs,
} from './dependency-checker.type';
